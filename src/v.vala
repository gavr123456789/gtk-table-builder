using Gtk;
using Gee;



namespace TableBuilder { 
    public delegate bool TableEvent (Gtk.Widget w, Gdk.EventButton e);
    public delegate void EditEvent (Gtk.CellRendererText w, string path, string new_text);
    
    struct Row {
        string[] values;
        //  ArrayList<string> values;
    }
    
    public int add(int a, int b){
        prin(@"a+b=$(a+b)");
        return a + b;
    }

    public class TreeViewBuilder: TreeView {
        //  public Gtk.TreeStore? tree_store = null;
        public Gtk.TreeStore? tree_store = null;

        ArrayList<Row?> rows = new ArrayList<Row?>();

        TreeViewColumn[] tree_view_colums;
        Gtk.CellRendererText[] cells = {};
        Type[] column_types = {};
        Gtk.TreeIter? last_iter;

        public TreeViewBuilder add_column (string title) {
            var new_cell = new CellRendererText();
            cells += new_cell;
            column_types += typeof(string);
            tree_view_colums += new TreeViewColumn.with_attributes (title,  new_cell, "text", tree_view_colums.length % 2);
            return this;
        } 

        public TreeViewBuilder add_row (params string[] row) {
            if (row.length != cells.length){
                error(@"The amount of content($(row.length)) does not match the number of columns($(cells.length))");
            }

            Row c = Row();
            foreach(var s in row)
            c.values += s;
            rows.add(c);
            return this;
        } 

        public TreeViewBuilder insert_new_row(TreeIter? parent, params string[] row ){
            Row tmp = Row();
            foreach(var a in row)
                tmp.values += a;
            
            rows.insert(tree_store.iter_n_children(parent) -1, tmp);
            return this;
        }

        //Cells
        public TreeViewBuilder cell_editable (int cell_pos/*, EditEvent e = editable_func*/) 
        requires (cell_pos < cells.length) {
            cells[cell_pos].editable = true;

            cells[cell_pos].edited.connect((w, path, new_text)=>{
                int local_column = cell_pos;
                TreeIter iter_current;
                bool iter_is_valid = model.get_iter(out iter_current, new Gtk.TreePath.from_string(path));
        
                if (iter_is_valid) {
                    prin(path," ",local_column, " ", new_text);
                    tree_store.set(iter_current, local_column, new_text);
                }
            });
            return this;
        }

        public TreeViewBuilder cell_ellipsize (int cell_pos, Pango.EllipsizeMode ellipsize = Pango.EllipsizeMode.END) {
            cells[cell_pos].ellipsize = ellipsize;
            return this;
        }
        public TreeViewBuilder cell_max_chars (int cell_pos, int max_chars = 10) {
            cells[cell_pos].max_width_chars = max_chars;
            cells[cell_pos].width_chars = max_chars;
            return this;
        }

        //Events
        public TreeViewBuilder add_event (TableEvent table_event ) {
            button_press_event.connect(table_event);
            return this;
        }


        public TreeViewBuilder add_right_click_popover(){

            button_press_event.connect((w,e)=>{
                var model_ref = this.model;

                if(e.button == 3){
                    var rect = Gdk.Rectangle()  {x = (int)e.x, y = (int)e.y + 25,
                        width = 1, height = 1 };
                    var pop  = new PopoverMenu(){pointing_to = rect, relative_to = w,
                        position = RIGHT, modal = true};
                    var box  = new Gtk.Box(Orientation.VERTICAL,0);
        
                    var itm_add_new    = new ModelButton(){text = "Add new"};
                    //  var itm_add_child  = new ModelButton(){text = "Add child"};
                    var itm_add_after  = new ModelButton(){text = "Insert after"};
                    var itm_add_before = new ModelButton(){text = "Insert before"};
                    var itm_delete     = new ModelButton(){text = "Delete selected"};
        
                    box.add(itm_add_new   );
                    //  box.add(itm_add_child );
                    box.add(itm_add_after );
                    box.add(itm_add_before);
                    box.add(itm_delete    );     
                    pop.add(box);
        
                    //  var tree_selection = this.get_selection();
                    TreeIter? cursor_iter;//tree_selection.get_selected(out model_ref, out selected_iter);
                    TreePath? tree_path = null;
                    TreeViewColumn? sas = null; // NO NEEDED
        
                    this.get_cursor(out tree_path, out sas);
                    prin(tree_path);
        
                    tree_store.get_iter(out cursor_iter, tree_path);
                    //  prin(tree_store.get_path(cursor_iter));
        
                    if(cursor_iter != null){
                        
                        if(cursor_iter != null){
                            
                            itm_add_new.clicked.connect(() => {
                                insert_new_row(null, "", "");
                            });
        
                            //  itm_add_child.clicked.connect(() => {
                            //      insert_new_row(selected_iter, "", "");
                            //  });
                            itm_add_after.clicked.connect(() => {
                                tree_store.insert_after(out cursor_iter, null, cursor_iter);
                                //  tree_store.set(cursor_iter, 0, "sas", 1, "Susмыслом");
                            });
                            itm_add_before.clicked.connect(() => {
                                tree_store.insert_before(out cursor_iter, null, cursor_iter);
                                //  tree_store.set(cursor_iter, 0, "asd", 1, "sas");
                            });
        
                            itm_delete.clicked.connect(() => {
                                tree_store.remove(ref cursor_iter);
                            });
        
                            pop.popup();
                            box.show_all();
                        }
                    }
                }
                return false;
            });

            
            return this;
        }

        //  public TableBuilder add_rigth_click_popover () {
        //      prin(this!=null);

        //      return this;
        //  }

        public TreeViewBuilder add_rigth_click_custom_menu (TableEvent table_event) {
            button_press_event.connect(table_event);
            return this;
        }

        public TreeViewBuilder add_on_click_lambda (TableEvent table_event) {
            button_press_event.connect(table_event);
            return this;
        }

        public TreeViewBuilder add_on_right_click_lambda () {
            return this;
        }

        //Imports
        public TreeViewBuilder import_from_XML () {
            error("Not implemented");
            return this;
        }
        public TreeViewBuilder import_from_JSON () {
            error("Not implemented");
            return this;
        }

        public TreeViewBuilder build () {
            foreach (var a in tree_view_colums){
                this.append_column(a);
            }
            //  for (int i = 0; i < tree_view_colums.length; i++){
            //      tree_view_colums[i].set_attributes();
            //      this.append_column(tree_view_colums[i]);
            //  }
            
            tree_store = new TreeStore.newv (column_types);

            prin(rows.size);
            foreach(var row in rows){
                prin(row.values[0], row.values[1] );
                TreeIter iter;
                tree_store.append(out iter, null);
                for (int i = 0; i < row.values.length; i++){
                    tree_store.set_value(iter, i, row.values[i]);
                    prin("set: ", row.values[i]," to ", i);
                }
            }
            
            this.set_model(tree_store);
            return this;
        }
    }
}
