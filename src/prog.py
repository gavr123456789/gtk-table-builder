#!/usr/bin/env python
import gi
gi.require_version("Gtk", "3.0")
# gi.require_version('TableBuilder', '0.2')
from gi.repository import Gtk
from gi.repository import GLib
from gi.repository import TableBuilder


if __name__ == "__main__":
    print('SAS')
    win = Gtk.Window()
    print(TableBuilder.add(1,2))
    builder = TableBuilder.TreeViewBuilder.new()
    builder.add_column("City").add_column("Capital").add_row("Russia", "Saint-Petersburg").add_row("Britain", "London").build()
    win.add(builder)
    win.connect("destroy", Gtk.main_quit)
    win.show_all()
    Gtk.main()